<?php
/**
 * Componente do cake para upload de arquivos
 * @author Tayron Miranda <falecom@tayronmiranda.com.br>
 * @param {array} File data
 * @return Nome do arquivo que foi TRUE ou retorna FALSE
 * @since 21/03/2012
 */
class UploadComponent extends Component {

	/**
	 * Define o tipo de arquivo permitido no upload
	 * @value {array} array( 'zip', 'jpg', 'rar' )
	 */
	private $type	= NULL;
	/**
	 * Armazena o nome que o arquivo devera receber
	 * @value {string} documento_abril
	 */
	private $newName = NULL;
	/**
	 * Armazena o nome do arquivo movido
	 * @value {string} nome do arquivo
	 */	
	private $uploadFileName = NULL;	
	/**
	 * Tamanho maximo que o arquivo pode ter em kbytes
	 * @value: {int} valor em kbytes
	 * @exemple: 10024
	 */	
	private $maxiSize	= NULL;
	/**
	 * Armazena o diretorio onde devera ser feito upload
	 * @value {string} documento_abril
	 */
	private $uploadDirectory = NULL;		
	/**
	 * Armazena request data file
	 * @value {array} request data file
	 */
	private $fileData = NULL;		
	
	
	/**
	 * Metodo que permite recuperar informacao do arquivo
	 * @return: {array} file data
	 */
	public function getFileData() 
	{
		return $this->_fileData;
	}

	
	/**
	 * Metodo que permite recuperar nome do arquivo upado
	 * @return: {string} Nome do arquivo upado
	 */
	public function getUploadFileName() 
	{
		return $this->_uploadFileName;
	}	
	
	
	/**
	 * Metodo que pega mensagem de erro
	 * @return: {string} Mensagem de erro
	 */
	public function getMessage() 
	{
		return $this->_fileData['error'];
	}	

	
	/**
	 * Metodo que seta array com tipo de arquivos permitidos
	 * @param:  {array} Array de tipos de arquivo
	 */
	public function setType( $type ) 
	{
		if( is_array( $type ) ){
			$this->_type = $type;
		}
		return $this;
	}		
	
	
	/**
	 * Metodo que pega mensagem de erro
	 * #param: {string} nome do diretorio para upload
	 * @return: {boolean} TRUE or FALSE
	 */
	public function setUploadDirectory( $directory ) 
	{
		$directory = '../webroot/img/upload/' . $directory;
		
		if( is_dir( $directory ) ){
			$this->_uploadDirectory = $directory;	
			@chmod( $directory , 0777 );
		}else{			
			if( mkdir( $directory, 0777 ) ){
				$this->_uploadDirectory = $directory;
			}else{
				$this->_fileData['error'] = 'Erro ao criar o diretorio';
				return FALSE;
			}
		}
		
		return TRUE;
	}		
	
	
	/**
	 * Metodo que gerencia o upload do arquivo
	 * @return {boolean} TRUE or FALSE
	 * @param: {array} request data file
	 */
	public function inicialize( $data )
	{
		// Setando file data 
		$this->_fileData = $data['file'];
		$this->_maxiSize = $data['file']['size'];
		
		// Criando diretorio para upload caso nao tenha criado
		$directory = ( isset( $data['directory'] ) ) ? $data['directory'] : 'upload';
		$this->setUploadDirectory( $directory );
				
		// Se o arquivo for uma imagem faço validacoes e upload
		if( $this->isImage() AND $this->sizeIsAllowed() AND $this->formatIsAllowed() ){
			return $this->processUpload();		
		}
		
		return FALSE;
	}

	
	/**
	 * Metodo que gerencia o processo de upload de arquivo
	 * @return {boolean} TRUE or FALSE
	 */	
	private function processUpload()
	{
		if( $this->_type != NULL AND $this->sizeIsAllowed() ){
			if( $this->formatIsAllowed() )
				return $this->upload();
		}else{
			if( $this->sizeIsAllowed() )
				return $this->upload();				
		}
		return FALSE;
	}	
	
	
	/**
	 * Metodo que verifica data type do arquivo para saber se ele e uma imagem 
	 * @return {boolean} TRUE or FALSE
	 */
	private function isImage()
	{		
		$typo = explode( '/', $this->_fileData['type'] );

		if( $typo[0] != 'image' ){
			$this->_fileData['error'] = 'O arquivo não é uma imagem';
			return FALSE;		
		}
		
		return TRUE;
	}
	
	
	/**
	 * Metodo que pega o formato da imagem
	 * @return {string} formato da imagem
	 */
	private function getFileType()
	{
		$type = explode( '.', $this->_image );
		return trim( strtolower( $type[1] ) );
	}

	
	/**
	 * Metodo que verifica se o formato do arquivo esta permitido para upload
	 * @return {boolean} TRUE or FALSE
	 */
	private function formatIsAllowed()
	{
		$data = str_replace( 'image/', '',  $this->_fileData['type'] );

		if( $this->_type != NULL ){

			if( array_search( $data, $this->_type ) === false ){
				$this->_fileData['error'] = 'O formato do arquivo nao é permitido para upload';
				return FALSE;							
			}
		}

		return TRUE;
	}
	
	
	/**
	 * Metodo que verifica se o formato do arquivo esta permitido para upload
	 * @return {boolean} TRUE or FALSE
	 */
	private function sizeIsAllowed()
	{
		if( $this->_fileData['size'] > $this->_maxiSize ){
			$this->_fileData['error'] = 'O tamanho do arquivo é superior ao permitido';
			return FALSE;						
		}
		
		return TRUE;
	}	
	
	
	/**
	 * Metodo que verifica se o diretorio onde vai ser feito upload existe e 
	 * verifica se nao ha um arquivo com o mesmo nome, senao altera o nome
	 * @return {boolean} TRUE or FALSE
	 */
	private function upload()
	{
		if( $this->_uploadDirectory != NULL ){
			
			if( file_exists( $this->getNameFile() ) ){
				$name = ( $this->isImage() ) ? 'imagem_': 'arquivo_';
				$this->_newName = $name . mktime() . date('dmYhis');
			}
			
			return $this->move();
		}
		
		$this->_fileData['error'] = 'Não foi informado diretorio para upload';
		return FALSE;
	}
	
	
	/**
	 * Metodo que move o arquivo do diretorio temporario para para o diretorio 
	 * indicado pelo usuario
	 * @return {boolean} TRUE or FALSE
	 */	
	private function move()
	{
		// Se o arquivo ja nao existir no servidor copio ele pro diretorio especificado
		if( move_uploaded_file( $this->_fileData['tmp_name'], $this->getNameFile() ) ){
			$this->_fileData['error'] = 'Arquivo upado com sucesso';				
			return TRUE;
		}
		
		$this->_fileData['error'] = 'Nao foi possivel mover o arquivo';				
		return FALSE;
	}
	

	/**
	 * Metodo que retira acentos de palavras
	 *
	 * @return string
	 */	
	 private function removeAcentWord( $string = null )
	 {
	 	if( $string != null ){
			
			$array1 = array( 'á', 'à', 'â', 'ã', 'ä', 'é', 'è', 'ê', 'ë', 'í', 'ì', 'î', 'ï', 'ó', 'ò', 
			'ô', 'õ', 'ö', 'ú', 'ù', 'û', 'ü', 'ç', 'Á', 'À', 'Â', 'Ã', 'Ä', 'É', 'È', 'Ê', 'Ë', 'Í', 
			'Ì', 'Î', 'Ï', 'Ó', 'Ò', 'Ô', 'Õ', 'Ö', 'Ú', 'Ù', 'Û', 'Ü', 'Ç' );

			$array2 = array( 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 
			'o', 'o', 'o', 'u', 'u', 'u', 'u', 'c', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'I', 
			'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'C' );

			return str_replace( $array1, $array2, $string ); 
	 		
	 	}else{

	 		$this->_fileData['error'] = 'Deve-se informar um texto, palavra para poder retirar os acentos';
	 		return $string;
	 	}
	 }
	

	/**
	 * Metodo que pega o nome do arquivo para ser usado no upload e 
	 * seta o nome usado numa variavel publica
	 */
	private function getNameFile()
	{
		if( $this->_newName != NULL ){
			
			$type = explode( '.', $this->_fileData['name'] );	
			$this->_uploadFileName = strtolower( ( $this->_newName ) ) . '.' . $type[1];
			$name = $this->removeAcentWord( strtolower ( $this->_newName ) ) . '.' . $type[1];
			
		}else{
			$this->_uploadFileName = time() . $this->removeAcentWord( strtolower( $this->_fileData['name'] ) );
			$name = time() . $this->removeAcentWord( strtolower( $this->_fileData['name'] ) );
		}
		
		return $this->_uploadDirectory . $name;		
	}
}